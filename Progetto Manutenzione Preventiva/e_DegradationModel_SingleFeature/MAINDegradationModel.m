%% Pulizia
% close all;
% clear;
% clc;

w = warning ('off','all');

load('../_data/Processed Datasets/AllDatasets.mat')
load('../_data/Processed Datasets/AllTestDatasets.mat');


%% DATASET LARGE
% TrainingData = datasetLargeData;
% TestData = datasetLargeTest;
%% DATASET SMALL
% TrainingData = datasetSmallData;
% TestData = datasetSmallTest;
%% DATASET LARGE + SMALL
TrainingData = datasetLargeSmallData;
TestData = datasetSmallLargeTest;

%% CALCOLO RESULTS
% for nFeature = 1:5
%     [errorSDTab{nFeature}, errorMeanTab{nFeature}, ...
%         errorMedianTab{nFeature}, TimeRulStimated{nFeature}] = ...
%     degradationModelFunction(TrainingData, TestData, nFeature); 
% end
%TimeRulStimated{nFeature}
%% Calcolo Errori 

% for nFeature = 1:5 
%     errorMeanTabLocal{nFeature} = errorMeanTab{nFeature}(nFeature); 
%     errorMedianTabLocal{nFeature} = errorMedianTab{nFeature}(nFeature); 
%     errorSDTabLocal{nFeature} = errorSDTab{nFeature}(nFeature); 
% end 
% 
% [MeanErrorMeanResult, MeanErrorMedianResult, MeanErrorSDResult] = checkError(errorMeanTabLocal', errorMedianTabLocal', errorSDTabLocal'); 
% disp('Mean Error');
% disp(MeanErrorMeanResult); 
% disp('Median Error');
% disp(MeanErrorMedianResult); 
% disp('SD Error');
% disp(MeanErrorSDResult);

%% Stima RUL 

for nFeature = 1:5 
    TimeRulStimatedLocal{nFeature} = TimeRulStimated{nFeature}(nFeature); 
end

MatrixTime = [TimeRulStimatedLocal{1,1}{1,1}...
    TimeRulStimatedLocal{1,2}{1,1}...
    TimeRulStimatedLocal{1,3}{1,1}...
    TimeRulStimatedLocal{1,4}{1,1}...
    TimeRulStimatedLocal{1,5}{1,1}...
]; 


for j = 1: height(MatrixTime)
    rowMatrix = MatrixTime(j,:); 
    meanTime(j,:) = mean(rowMatrix);
end 


figure
plot(meanTime);
xlabel('seconds')
ylabel('RUL Estimated')
title('RUL Mean across 5 features')
legend('Mean Time Estimated RUL')