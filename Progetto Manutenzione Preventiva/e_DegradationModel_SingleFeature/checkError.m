function [MeanErrorMeanResult, MeanErrorMedianResult, MeanErrorSDResult] = checkError(errorMeanTab, errorMedianTab, errorSDTab)
    %% Error Mean Tab 
    SumErrorMean50 = 0; 
    SumErrorMean70 = 0; 
    SumErrorMean90 = 0; 
    for I=1:5 
        row = errorMeanTab{I};
        row = row{1};
        SumErrorMean50 = SumErrorMean50 + row(1); 
        SumErrorMean70 = SumErrorMean70 + row(2); 
        SumErrorMean90 = SumErrorMean90 + row(3); 
    end 
    
    MeanErrorMean50 = SumErrorMean50/5; 
    MeanErrorMean70 = SumErrorMean70/5;
    MeanErrorMean90 = SumErrorMean90/5;

    MeanErrorMeanResult = [MeanErrorMean50, MeanErrorMean70, MeanErrorMean90]; 
    
    %% Error Median Tab 
    SumErrorMedian50 = 0; 
    SumErrorMedian70 = 0; 
    SumErrorMedian90 = 0; 
    for I=1:5 
        row = errorMedianTab{I};
        row = row{1};
        SumErrorMedian50 = SumErrorMedian50 + row(1); 
        SumErrorMedian70 = SumErrorMedian70 + row(2); 
        SumErrorMedian90 = SumErrorMedian90 + row(3); 
    end 
    
    MeanErrorMedian50 = SumErrorMedian50/5; 
    MeanErrorMedian70 = SumErrorMedian70/5;
    MeanErrorMedian90 = SumErrorMedian90/5;
    
    MeanErrorMedianResult = [MeanErrorMedian50, MeanErrorMedian70, MeanErrorMedian90];
    
    %% Error SD Tab 
    SumErrorSD50 = 0; 
    SumErrorSD70 = 0; 
    SumErrorSD90 = 0; 
    
    for I=1:5 
        row = errorSDTab{I};
        row = row{1};
        SumErrorSD50 = SumErrorSD50 + row(1); 
        SumErrorSD70 = SumErrorSD70 + row(2); 
        SumErrorSD90 = SumErrorSD90 + row(3); 
    end 
    
    MeanErrorSD50 = SumErrorSD50/5; 
    MeanErrorSD70 = SumErrorSD70/5;
    MeanErrorSD90 = SumErrorSD90/5;

    MeanErrorSDResult = [MeanErrorSD50, MeanErrorSD70, MeanErrorSD90];
end
