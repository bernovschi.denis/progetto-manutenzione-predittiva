# Progetto-Manutenzione-Predittiva

Progetto di Manutenzione Predittiva, tramite la stima della RUL (Remaining Useful Lifetime). Il progetto è stato sviluppato insieme a E. I. durante il corso di "Preventive Maintenance for Robotics and Smart Automation"

## Relazione 
[Relazione](https://gitlab.com/bernovschi.denis/progetto-manutenzione-predittiva/-/blob/c8c89d2b9a44033f3498bed4026cb8b148104d11/Relazione_Manutenzione.pdf)

